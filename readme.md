# No, Thanks!

A bot tournament system for the tabletop game by Thorsten Gimmler

## Environment Setup

1. Fork, clone, or download this git repo
2. Install deno CLI from https://deno.com/runtime
3. Open the root folder in VS Code
4. Install "Deno" extension for VS Code
5. Open command palette (Ctrl+Shift+P) and execute "Deno: initialize workspace"

## Development Guide

1. Copy `BasicBot.ts` into a new file
2. Update `debug.ts` to include your new bot in the test game
3. Run `deno run debug.ts` to see your bot in action
4. Customize the logic in your new bot file
5. Customize `debug.ts` to experiment with different game settings
6. Push or upload your project to a publicly available location. If you upload
   just your bot file, its imports should all be http references to this
   project.
7. Send me the link to your raw bot file, like:
   https://gitlab.com/bquinn_precoa/no-thanks/-/raw/main/bots/BasicBot.ts

## Bot Restrictions

1. Do not output to or read from the console
2. Do not make web requests
3. All methods are synchronous
4. Don't read the source code of other bots (except the examples)
