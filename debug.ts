import BasicBot from "./bots/BasicBot.ts";
import BasicBotJS from "./bots/BasicBot.js";
import HumanBot from "./bots/HumanBot.ts";
import QuickBot from "./bots/QuickBot.ts";
import Game from "./noThanks/Game.ts";
import { DumpUIDriver } from "./noThanks/ui/DumpUIDriver.ts";
import { RealtimeUIDriver } from "./noThanks/ui/RealtimeUIDriver.ts";

// Pick one of the following UI drivers:
// const uiDriver = DumpUIDriver; // Spit out the results of the whole game at once
const uiDriver = RealtimeUIDriver; // Step through the game turn-by-turn

const game = new Game(new uiDriver());

// Edit this players array to customize your test game
const players = [
  new BasicBot(1),
  new QuickBot(2),
  // new HumanBot(3), // Uncomment to play the game yourself
  // new BasicBotJS(3),
  // new QuickBot(3),
  // new QuickBot(4),
  // new QuickBot(5),
];

game.play(players);
