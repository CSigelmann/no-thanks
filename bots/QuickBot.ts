import { Bot } from "../noThanks/Types.d.ts";

/**
 * QuickBot: Picks up any card with two or more tokens on it
 */
export default class implements Bot {
  public name = "Quick Bot";
  public id: number;

  private tokens = 0;

  constructor(id: number) {
    this.id = id;
  }

  public beginGame(
    _playerNumber: number,
    _playerCount: number,
    tokens: number,
  ): void {
    this.tokens = tokens;
  }

  public reportTurn(
    _playerNumber: number,
    _card: number,
    _tokens: number,
    _taken: boolean,
  ): void {
    ///
  }

  public playTurn(card: number, tokens: number): boolean {
    // If we are out of tokens then we must pick up the card
    if (this.tokens === 0) {
      // Yes please!
      this.tokens += tokens;
      return true;
    }

    // If there are already a couple tokens down let's keep things moving
    // by grabbing that card!
    if (tokens >= 2) {
      this.tokens += tokens;
      return true;
    }

    // No thanks! I'll spend a token to pass
    this.tokens--;
    return false;
  }
}
