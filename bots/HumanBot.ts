import { Bot } from "../noThanks/Types.d.ts";
import { Sort } from "../noThanks/Utilities.ts";

/**
 * Human Bot: Prompts the console for decision-making
 */
export default class implements Bot {
  public name = "Human Bot";
  public id: number;

  private tokens = 0;
  private cards: Array<number> = [];

  constructor(id: number) {
    this.id = id;
    this.name = prompt("Enter your name") || "Human Bot";
  }

  public beginGame(
    _playerNumber: number,
    _playerCount: number,
    tokens: number,
  ): void {
    this.tokens = tokens;
  }

  public reportTurn(
    _playerNumber: number,
    _card: number,
    _tokens: number,
    _taken: boolean,
  ): void {
    ///
  }

  public playTurn(card: number, tokens: number): boolean {
    console.log("Your hand: ", this.cards.join(", "));
    const choice = prompt(`You have ${this.tokens} tokens. Take card? (y/N)`) ||
      "n";

    // Yes please!
    if (this.tokens <= 1 || ["y", "Y"].includes(choice)) {
      this.tokens += tokens;
      this.cards.push(card);
      this.cards = Sort(this.cards);
      return true;
    }

    // No thanks!
    this.tokens--;
    return false;
  }
}
