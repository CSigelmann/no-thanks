import { UIDriver } from "../Types.d.ts";

type EventType = "printLine" | "pause" | "clear";

interface UiEvent {
  eventType: EventType;
  messages?: Array<string>;
}

/**
 * A class to capture UI to be replayed at a later time
 */
export class ReplayUIDriver implements UIDriver {
  private events: Array<UiEvent> = [];

  private pushEvent(eventType: EventType) {
    this.events.push({
      eventType: eventType,
    });
  }

  private pushPrintLineEvent(messages: Array<string>) {
    this.events.push({
      eventType: "printLine",
      messages: messages,
    });
  }

  public printLine(...messages: Array<string>): void {
    this.pushPrintLineEvent(messages);
  }

  public pause(): void {
    this.pushEvent("pause");
  }

  public clear(): void {
    this.pushEvent("clear");
  }

  // Replay all the captured events
  public replay(driver: UIDriver) {
    while (this.events.length > 0) {
      const event = this.events.shift();

      switch (event?.eventType) {
        case "clear":
          driver.clear();
          break;

        case "pause":
          driver.pause();
          break;

        case "printLine":
          driver.printLine(...event.messages || []);
          break;
      }
    }
  }
}
