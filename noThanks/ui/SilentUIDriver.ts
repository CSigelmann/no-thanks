import { UIDriver } from "../Types.d.ts";

export class SilentUIDriver implements UIDriver {
  public printLine(..._messages: Array<string>): void {
  }

  public pause(): void {
  }

  public clear(): void {
  }
}
