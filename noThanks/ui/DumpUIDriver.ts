import { UIDriver } from "../Types.d.ts";

export class DumpUIDriver implements UIDriver {
  public printLine(...messages: Array<string>): void {
    console.log(messages.join(" "));
  }

  public pause(): void {
    console.log();
  }

  public clear(): void {
    console.log();
  }
}
