import { assertEquals } from "https://deno.land/std@0.190.0/testing/asserts.ts";

import { ScoreCards } from "../Utilities.ts";

Deno.test("Score utility", () => {
  const cards = [3, 9, 7, 8];
  assertEquals(10, ScoreCards(cards));
});
