import { assertEquals } from "https://deno.land/std@0.190.0/testing/asserts.ts";
import {
  detectDuplicates,
  findPerfectReplay,
  GameReplay,
  segmentArray,
} from "../tourney.ts";
import { ReplayUIDriver } from "../ui/ReplayUIDriver.ts";

Deno.test("detectDuplicates", () => {
  assertEquals(false, detectDuplicates([1, 5, 2, 7, 6]));
  assertEquals(true, detectDuplicates([1, 5, 2, 7, 5]));
});

Deno.test("findPerfectReplay", () => {
  const target = [1, 2, 0];
  const replays: Array<GameReplay> = [
    {
      finishingOrder: [0, 2, 1],
      replay: new ReplayUIDriver(),
    },
    {
      finishingOrder: [0, 1, 1],
      replay: new ReplayUIDriver(),
    },
    {
      finishingOrder: [1, 2, 0],
      replay: new ReplayUIDriver(),
    },
  ];

  const result = findPerfectReplay(target, replays);

  assertEquals(1, result?.finishingOrder[0]);
  assertEquals(2, result?.finishingOrder[1]);
  assertEquals(0, result?.finishingOrder[2]);
});

Deno.test("segmentArray", () => {
  assertEquals([[1, 2, 3]], segmentArray([1, 2, 3]));
  assertEquals([[1, 2, 3, 4]], segmentArray([1, 2, 3, 4]));
  assertEquals([[1, 2, 3, 4, 5]], segmentArray([1, 2, 3, 4, 5]));
  assertEquals([[1, 2, 3], [4, 5, 6]], segmentArray([1, 2, 3, 4, 5, 6]));
  assertEquals([[1, 2, 3, 4], [5, 6, 7]], segmentArray([1, 2, 3, 4, 5, 6, 7]));
  assertEquals(
    [[1, 2, 3, 4], [5, 6, 7, 8]],
    segmentArray([1, 2, 3, 4, 5, 6, 7, 8]),
  );
  assertEquals(
    [[1, 2, 3, 4], [5, 6, 7, 8, 9]],
    segmentArray([1, 2, 3, 4, 5, 6, 7, 8, 9]),
  );
  assertEquals(
    [[1, 2, 3, 4], [5, 6, 7], [8, 9, 10]],
    segmentArray([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
  );
  assertEquals(
    [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11]],
    segmentArray([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]),
  );
  assertEquals(
    [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]],
    segmentArray([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]),
  );
  assertEquals(
    [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12, 13]],
    segmentArray([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]),
  );
});
