import { PlayerState } from "./Types.d.ts";

/**
 * Given an array of card values, return the total score
 * Remember that a players score will also be reduced by their number of tokens
 */
export function ScoreCards(cards: Array<number>): number {
  // Don't score a card if we are also holding the previous value
  const scored: Array<number> = cards.filter((value) =>
    !cards.includes(value - 1)
  );

  return scored.reduce((sum, value) => sum + value, 0);
}

/**
 * Return the player's current score
 */
export function ScorePlayer(player: PlayerState): number {
  return ScoreCards(player.cards) - player.tokens;
}

/**
 * Return a random number in the given range, including bounds
 */
export function RandomInt(minInclusive: number, maxInclusive: number): number {
  const range: number = maxInclusive - minInclusive + 1;
  return Math.floor(Math.random() * range) + minInclusive;
}

/**
 * Returns a sorted shallow copy of the given array
 */
export function Sort(list: Array<number>): Array<number> {
  const copy = list.slice();
  copy.sort((a, b) => a - b);
  return copy;
}

/**
 * Return a scrambled shallow copy of the given array
 */
export function Shuffle<T>(list: Array<T>): Array<T> {
  const input = list.slice();
  const output: Array<T> = [];

  while (input.length > 0) {
    const randIndex = RandomInt(0, input.length - 1);
    output.push(input[randIndex]);
    input.splice(randIndex, 1);
  }

  return output;
}
