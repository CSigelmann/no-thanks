import { Shuffle } from "./Utilities.ts";

// A deck of cards in No Thanks has one card each with values 3-35
export default class Deck {
  private cards: Array<number>;

  constructor() {
    this.cards = [];
    for (let value = 3; value <= 35; value++) {
      this.cards.push(value);
    }
  }

  public draw(): number | undefined {
    return this.cards.shift();
  }

  public shuffle(): void {
    this.cards = Shuffle(this.cards);
  }

  public toss(numToToss: number): void {
    for (let count = numToToss; count > 0; count--) {
      this.cards.shift();
    }
  }

  public size() {
    return this.cards.length;
  }
}
