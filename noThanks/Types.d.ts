export interface Bot {
  id: number;
  name: string;
  beginGame(playerNumber: number, playerCount: number, tokens: number): void;

  /** Optionally run bookkeeping for a turn that happened in the game */
  reportTurn(
    playerNumber: number,
    card: number,
    tokens: number,
    taken: boolean,
  ): void;

  /** Decide whether to take the offered card */
  playTurn(card: number, tokens: number): boolean;
}

export type BotFactory = { new (id: number): Bot };

export interface PlayerState {
  cards: Array<number>;
  tokens: number;
}

export interface UIDriver {
  printLine(...message: Array<string>): void;
  pause(): void;
  clear(): void;
}
