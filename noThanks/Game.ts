import Deck from "./Deck.ts";
import { Bot, PlayerState, UIDriver } from "./Types.d.ts";
import { ScorePlayer, Sort } from "./Utilities.ts";
import {
  colorActive,
  colorBold,
  colorNo,
  colorRelevantCard,
  colorScoringCard,
  colorUnscoringCard,
  colorYes,
} from "./ui/colors.ts";

interface PlayerSeat {
  state: PlayerState;
  brain: Bot;
  score: number;
}

export interface GameSeatResult {
  id: number;
  score: number;
}

export default class Game {
  private deck: Deck;
  private players: Array<PlayerSeat> = [];
  private numCardsToToss = 9;
  private numTokens = 55;
  private ui: UIDriver;

  constructor(uiDriver: UIDriver) {
    this.deck = new Deck();
    this.ui = uiDriver;
  }

  /**
   * Run the game until it ends
   */
  public play(bots: Array<Bot>): Array<GameSeatResult> {
    let currentPlayerIndex = 0;
    let topCard: number | undefined = undefined;
    let tokens = 0;
    let decision = false;

    // Game setup
    this.players = this.createPlayers(bots);
    this.deck.shuffle();
    this.deck.toss(this.numCardsToToss);
    topCard = this.deck.draw();

    // Game loop
    while (topCard !== undefined) {
      // Write current player thinking time to UI
      this.displayGame(topCard, tokens, currentPlayerIndex);
      this.ui.pause();

      decision = this.players[currentPlayerIndex].brain.playTurn(
        topCard,
        tokens,
      );

      // Write outcome to UI
      this.displayGame(topCard, tokens, currentPlayerIndex, decision);
      this.ui.pause();

      // Send outcome to all players
      this.players.forEach((p) =>
        p.brain.reportTurn(
          currentPlayerIndex,
          topCard as number,
          tokens,
          decision,
        )
      );

      if (decision) {
        // Player picks up card and tokens
        this.players[currentPlayerIndex].state.cards.push(topCard);
        this.players[currentPlayerIndex].state.tokens += tokens;

        tokens = 0;
        topCard = this.deck.draw();

        // The current player is still active for the next card.
      } else {
        // Player puts down a token
        if (this.players[currentPlayerIndex].state.tokens < 1) {
          throw new Error(
            this.players[currentPlayerIndex].brain.name +
              " tried to spend a token they do not have",
          );
        }

        this.players[currentPlayerIndex].state.tokens--;
        tokens++;

        // Go to the next player
        currentPlayerIndex = (currentPlayerIndex + 1) % this.players.length;
      }
    }

    this.ui.clear();
    this.ui.printLine("*** Final Scoring ***");
    this.ui.pause();

    // Final scoring
    for (let i = 0; i < this.players.length; i++) {
      this.players[i].score = ScorePlayer(this.players[i].state);
      this.displayHand(this.players[i]);
      this.ui.pause();
    }

    const places = this.players.slice();
    places.sort((a, b) => a.score - b.score);

    this.displayResults(places);

    return places.map((seat) => ({
      id: seat.brain.id,
      score: seat.score,
    }));
  }

  /** Given an ordered list of players, draw the final results */
  private displayResults(places: Array<PlayerSeat>) {
    this.ui.printLine();

    for (let i = 0; i < places.length; i++) {
      this.ui.printLine(places[i].brain.name, places[i].score.toString());
    }
  }

  private displayGame(
    topCard: number,
    tokens: number,
    currentPlayerIndex: number,
    decision: boolean | undefined = undefined,
  ) {
    this.ui.clear();

    if (this.deck.size() > 0) {
      this.ui.printLine(this.deck.size().toString() + " cards left");
    } else {
      this.ui.printLine("Final card!");
    }
    this.ui.printLine();

    this.displayTable(topCard, tokens);

    this.ui.printLine();

    if (decision === undefined) {
      this.ui.printLine();
    } else {
      this.ui.printLine(
        decision ? colorYes("Yes Please!") : colorNo("No Thanks!"),
      );
    }

    this.ui.printLine();

    this.players.forEach((p, i) =>
      this.displayHand(p, i === currentPlayerIndex, topCard)
    );

    this.ui.printLine();
    this.ui.printLine();
  }

  /** Display the top card and number of tokens on the table */
  private displayTable(topCard: number, tokens: number) {
    this.ui.printLine(colorBold(topCard.toString()));
    this.ui.printLine("*".repeat(tokens));
  }

  /** Display a user's cards */
  private displayHand(player: PlayerSeat, active = false, topCard = 0) {
    const cards = Sort(player.state.cards.slice());

    const name = active ? colorActive(player.brain.name) : player.brain.name;
    const formattedCards = cards.map((c, i) => {
      let formatter = colorScoringCard;
      let separator = " + ";

      if (cards.includes(c - 1)) {
        separator = ",";
        formatter = colorUnscoringCard;
      }

      if (Math.abs(topCard - c) === 1) {
        formatter = colorRelevantCard;
      }

      return formatter(separator + c.toString());
    });

    this.ui.printLine(
      `${name}: (${player.state.tokens}) ${formattedCards.join("")} => ${
        ScorePlayer(player.state)
      }`,
    );
  }

  /** Set up players for the match */
  private createPlayers(bots: Array<Bot>) {
    const tokensPerPlayer = Math.min(
      11,
      Math.floor(this.numTokens / bots.length),
    );

    // Create players
    const players = bots.map((bot) => ({
      state: {
        cards: [],
        tokens: tokensPerPlayer,
      },
      brain: bot,
      score: 0,
    }));

    // Init players
    players.forEach((p, index) =>
      p.brain.beginGame(index, bots.length, tokensPerPlayer)
    );

    return players;
  }
}
